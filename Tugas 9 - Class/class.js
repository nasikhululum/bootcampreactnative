console.log('\n============================');
console.log('1. Animal Class. Release 0 ');
console.log('============================\n');

class Animal {
	
	constructor(animalname, animallegs = 4, animalcoldblood = false) {
		this._anname = animalname;
		this._anlegs = animallegs;
		this._ancold_blooded = animalcoldblood;
	}
	get name() {
		return this._anname;
	}
	set name(x) {
		this._name = x;
	}
	get legs() {
		return this._anlegs;
	}
	set legs(y) {
		this._legs = y;
	}
	get cold_blooded() {
		return this._ancold_blooded;
	}
	set cold_blooded(z) {
		this._cold_blooded = z;
	}
	
}

 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n============================');
console.log('1. Animal Class. Release 1 ');
console.log('============================\n');

class Ape extends Animal {
  constructor(animalname, animallegs = 2, animalcoldblood = true) {
    super(animalname, animallegs, animalcoldblood);
  }
  yell() {
    const x = 'Auooo';
	console.log (x);
  }
}

class Frog extends Animal {
  constructor(animalname, animallegs = 4, animalcoldblood = false) {
    super(animalname, animallegs, animalcoldblood);
  }
  jump() {
    const x = 'hop hop';
	console.log (x);
  }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
// console.log(sungokong.name) 
// console.log(sungokong.legs) 
// console.log(sungokong.cold_blooded, '\n') 
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
// console.log(kodok.name) 
// console.log(kodok.legs) 
// console.log(kodok.cold_blooded) 

console.log('\n============================');
console.log('2. Function to Class ');
console.log('============================\n');

class Clock {
	
	constructor({ template }) {
		this.template = template;
	}
		
	render() {
		let date = new Date();

		let hours = date.getHours();
		if (hours < 10) hours = '0' + hours;

		let mins = date.getMinutes();
		if (mins < 10) mins = '0' + mins;

		let secs = date.getSeconds();
		if (secs < 10) secs = '0' + secs;

		let output = this.template
		  .replace('h', hours)
		  .replace('m', mins)
		  .replace('s', secs);

		console.log(output);
	}
	
	stop() {
		clearInterval(this.timer);
	};

	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	};
	
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

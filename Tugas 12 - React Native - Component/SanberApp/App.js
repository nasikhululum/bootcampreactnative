
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram';

export default function App() {
  return (
      <Telegram />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Post from './Tugas/Tugas14/Post';

export default function App() {
  return (
      <View>
        <ScrollView>
          <Post />
        </ScrollView>
      </View>
      
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import React from 'react'
import { View, Text, Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput, Button } from 'react-native'

export default function Login() {

    return (
        <View style={styles.container}>
            <View style={styles.logoHeader}>
                <Image 
                    source={require('./asset/logo.png')}
                />
            </View>

            <Text style={styles.headerTitle}>Login</Text>

            <View style={styles.Inputan}>
                <Text style={styles.labelInput}>Username</Text>    
            </View> 
            <View style={styles.Inputan}>
            <TextInput
                    style={styles.Texti}
                    placeholder= "Masukkan Username"
                /> 
            </View> 

            <View style={styles.Inputan}>
                <Text style={styles.labelInput}>Password</Text>
            </View> 
            <View style={styles.Inputan}>
            <TextInput
                    style={styles.Texti}
                    placeholder= "Masukkan Password"
                    secureTextEntry={true} 
                /> 
            </View> 
            
            <View>
                <TouchableOpacity style = {styles.submitButton} >
                <Text style = {styles.submitButtonText}> Daftar </Text>
                </TouchableOpacity>

                <Text style={{textAlign:"center"}}>Atau</Text>

                <TouchableOpacity style = {styles.loginButton} >
                <Text style = {styles.submitloginText}> Masuk </Text>
                </TouchableOpacity>
            </View>
            

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    headerTitle:{
        marginVertical: 25,
        paddingHorizontal: 10,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        fontSize: 18,
    },
    logoHeader: {
        height: 50,
        backgroundColor: 'white',
        marginTop: 50,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 25
    },
    Inputan:{
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingHorizontal: 10,
        justifyContent: "flex-start",
        alignItems: 'center'
    },
    labelInput:{
        fontSize: 14,
    },
    Texti:{
        width: 338,
        borderWidth: 1,
        marginBottom:5,
        paddingHorizontal: 2,
        borderRadius: 3,
    },
    submitButton: {
        backgroundColor: '#003466',
        padding: 10,
        margin: 10,
        height: 40,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 7,
        borderWidth: 1,
     },
     submitButtonText:{
        color: 'white',
        textAlign: "center",
        fontSize: 14,
     },
     loginButton: {
        backgroundColor: 'white',
        padding: 10,
        margin: 10,
        height: 40,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 7,
        borderWidth: 1,
        borderColor: '#003466'
     },
     submitloginText:{
        color: '#003466',
        textAlign: "center",
        fontSize: 14,
        fontWeight: "bold"
     }

  });
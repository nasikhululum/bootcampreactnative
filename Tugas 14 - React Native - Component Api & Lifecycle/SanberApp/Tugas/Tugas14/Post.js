import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TextInput, View, Button, Line, TouchableOpacity } from 'react-native'
import Axios from 'axios'

const Item = ({title, value, onPress, onDelete}) => {
    return (
        
        <View style={styles.itemcontainer}>
            
                <View style={styles.desc}>
                    <TouchableOpacity onPress={onPress}>
                        <Text style={styles.descTitle}>{title}</Text>
                        <Text style={styles.descValue}>{value}</Text>
                    </TouchableOpacity>
                </View>    
                <TouchableOpacity onPress={onDelete}>
                    <Text style={styles.delete}>X</Text>
                </TouchableOpacity>
        </View>
    )
}

const Api = () => {

    const [title, setTitle] = useState("");
    const [val, setVal] = useState("");
    const [details, setDetail] = useState([]);
    const [button, setButton] = useState("Simpan");
    const [selectedid, setSelectedid] = useState({})

    useEffect(() => {
        getData();
    }, [])

    const submit = () => {
        const data = {
            title,
            value: val
        }
        if(button === 'Simpan'){
            
            console.log('data before send', data)
            Axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news', data)
            .then(res => {
                console.log('res: ', res)
                setTitle("")
                setVal("")
                getData()
            })
        }else if(button === 'Update'){
            Axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedid.id}`, data)
            .then(res => {
                console.log('res update: ', res)
                setTitle("")
                setVal("")
                getData()
                setButton("Simpan")
            })
        }
    }

    const getData = () => {
        Axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res => {
            // const data1= (res.data.data)
            console.log('res: ', res.data.data)
            setDetail(res.data.data)
        })
    }

    const selectItem = (item) => {
            console.log('Selected item :', item)
            setSelectedid(item)
            setTitle(item.title)
            setVal(item.value)
            setButton("Update")
    }

    const deleteItem = (item) => {
        console.log('Deleted item :', item)
        Axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
        .then(res => {
            console.log('res delete : ', res)
            setTitle("")
            setVal("")
			setButton("Simpan")
            getData()
        })
    }

    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>API (JSON Server)</Text>
            <Text style={{paddingBottom:5, textAlign:"center"}}>Input Data API</Text>
            <TextInput placeholder="Title" style={styles.input} value={title} onChangeText={(value) => setTitle(value)}></TextInput>
            <TextInput placeholder="Value" style={styles.input} value={val} onChangeText={(value) => setVal(value)}></TextInput>
            <Button title={button} onPress={submit}></Button>
            <View style={styles.line} />

            
            {details.map(detail => {
                    return <Item 
                    key={detail.id}
                    title={detail.title} 
                    value={detail.value} 
                    onPress={() => selectItem(detail)}
                    onDelete={() => deleteItem(detail)}
                    />
            })}
            
        </View>
    )
}

export default Api

const styles = StyleSheet.create({
    container: {padding: 20},
    textTitle: {textAlign: 'center', marginBottom: 20, fontSize: 16},
    line: {height:2, backgroundColor:'black', marginVertical:20},
    input: {borderWidth:1, marginBottom:12, borderRadius:25, paddingHorizontal:18},
    itemcontainer: {flexDirection: "row", marginBottom: 20},
    desc: {marginLeft: 18, flex:1},
    descTitle: {fontSize:20, fontWeight:"bold"},
    descValue: {fontSize:18, fontStyle: "italic"},
    delete: {fontSize:20, fontWeight:"bold", color: "red"}
})

console.log('\n===================');
console.log('Tugas 4 – Functions');
console.log('===================\n');


//NO. 1
function teriak() {
  return ("Halo Sanbers!")
}
 
console.log('No. 1 '+teriak())


// NO. 2
function kalikan(num1, num2) {
  return num1 * num2
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log('No. 2 '+hasilKali) // 48

// No. 3
var introduce = function(name, age, address, hobby) {
  return ("Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!")
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log("No. 3 "+perkenalan)
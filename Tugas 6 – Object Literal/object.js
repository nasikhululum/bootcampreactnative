
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function getAge(year=null){
	
	if (year==null || year>thisYear){
		return Age = 'Invalid Birth Year'
	}else{
		return Age = (thisYear - year)
	}
}

function arrayToObject(arr = '') {
	
    var obj = [];
	var obj_detail = {};
	var no = 1;
	
    for (var i = 0; i < arr.length; ++i){
		
		var obj_detail= {
			firstName : arr[i][0],
			lastName : arr[i][1],
			gender : arr[i][2],
			age : getAge(arr[i][3])
		}
		
		var myKey = no+'. '+arr[i][0]+" "+arr[i][1];
        obj[myKey] = obj_detail;
		no++
		var obj_detail= {};
    }
	
    console.log(obj);
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]


console.log('\n=============================');
console.log('Soal No. 1 (Array to Object)');
console.log('=============================\n');

console.log('1.a) ')
arrayToObject(people);
console.log('1.b) ')
arrayToObject(people2);
console.log('1.c) ')
arrayToObject();


function shoppingTime(memberId='', money=0) {
	
	if(memberId=='' && money==0){
		var x = 'Mohon maaf, toko X hanya berlaku untuk member saja'
		return x
	}else if(memberId==''){
		var x = 'Mohon maaf, toko X hanya berlaku untuk member saja'
		return x
	}else if(money<50000){
		var y = 'Mohon maaf, uang tidak cukup'
		return y
	}
	
	var obj_detail = {}
	var x = memberId
	var y = money
	var total_bayar = 0;
	var detail_barang = []
	var harga = 0;
	
	var listitem = [
		["Sepatu brand Stacattu", 1500000],
		["Baju brand Zoro", 500000],
		["Baju brand H&N", 250000],
		["Sweater brand Uniklooh", 175000],
		["Casing Handphone", 50000],
	]
	
	for (var i = 0; i < listitem.length; ++i){
		
		if(listitem[i][1]<=y){
			
			if(listitem[i][1]!='undefined'){
				var harga = parseInt(listitem[i][1]);
			}
			detail_barang.push(listitem[i][0]);
		}
		
		total_bayar += (harga)
		
		// console.log(harga)
    }
	
	var kembalian = y-total_bayar
	
	var obj_detail= {
			memberId : x,
			money : y, 
			listPurchased : detail_barang,
			changeMoney: kembalian
		}
	
	return obj_detail
	
}

console.log('\n=============================');
console.log('Soal No. 2 (Shopping Time)');
console.log('=============================\n');

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log('2.a) ')
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log('2.b) ')
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log('2.c) ')
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n=============================');
console.log('Soal No. 3 (Naik Angkot)');
console.log('=============================\n');

function naikAngkot(arrPenumpang) {

	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	var angkot = [{},{}];
	var i=0;
	var asal = '';
	var tujuan = '';
  
	for (i; i<arrPenumpang.length; i++) {
		var j = 0;
  
		for (j; j<arrPenumpang[i].length; j++) {
			
			switch (j) {
				case 0: {
					angkot[i].penumpang = arrPenumpang[i][j];
					break;
				} case 1: {
					angkot[i].naikDari = arrPenumpang[i][j];
					angkot[i].tujuan = arrPenumpang[i][j+1];
					break;
				} case 2: {
					asal = arrPenumpang[i][j-1];
					tujuan = arrPenumpang[i][j];
					var jarak = 0;
					for (var k=0; k<rute.length; k++) {
						if (rute[k] === asal) {
							for (var l=k+1; l<rute.length; l++) {
								jarak += 1;
								if (rute[l] === tujuan) {
									var bayar = jarak * 2000;
									angkot[i].bayar = bayar;
								}
							}
						}
					}
					break;
				}
			}
		}
	}

  return angkot;
  }
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  
  // console.log(naikAngkot([])); //[]

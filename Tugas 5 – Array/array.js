console.log('\n===================');
console.log('Soal No. 1 (Range) ');
console.log('===================\n');

var array = [];
var array1 = [];

function range(start = '', end = '') {
        
    if(end > start){
        var length = end - start; 

        for (var i = 0; i <= length; i++) { 
            array[i] = start;
            start++;
        }
        
        return array;
        
    }else if(start == '' || end == ''){
        
        array = [-1];
        return array;
         
    }else if(start > end){
        
        var length = start - end;  
       
        for (var i = 0; i <= length; i++) { 
            array1[i] = end;
            end++;
        }
        
        array1.sort(function (value1, value2) { return value2 - value1 } ) ; 
        return array1;
        
    }

    
}
console.log("1. ")
console.log(range(1, 10))

console.log("\n2. ")
console.log(range(1))

console.log("\n3. ")
console.log(range(11,18))

console.log("\n4. ")
console.log(range(54, 50))

console.log("\n5. ")
console.log(range())

console.log('\n=============================');
console.log('Soal No. 2 (Range with Step)');
console.log('=============================\n');

function rangeWithStep(start, end, step) {
  if (step === undefined) step = 1;
  var array = [];
  
  if (end > start) {
    for (var i = start; i <= end; i += step)
      array.push(i);
  } else if(start > end) {
    for (var i = start; i >= end; i -= step)
      array.push(i);
  }
  
  return array;
}

console.log("\n1. ")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log("\n2. ")
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log("\n3. ")
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log("\n4. ")
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('\n=============================');
console.log('Soal No. 3 (Sum of Range)');
console.log('=============================\n');


function sum(start='', end='', step='') {
    var total = 0;
    var arr = [];
    
    if(step==''){
        var start1 = Math.min(start);
        var end1 = Math.max(end);
        
        if (end > start) {
            for (var i = start1; i <= end1; i++)
              arr.push(i);
        } else if(start > end) {
            for (var i = start1; i >= end1; i--)
              arr.push(i);
        }
        
    }else if(step!=''){
        
        if (end > start) {
            for (var i = start; i <= end; i += step)
              arr.push(i);
        } else if(start > end) {
            for (var i = start; i >= end; i -= step)
              arr.push(i);
        }
    }else if(start=='1' && end=='' && step==''){
            arr.push = [1];
    }
    
    for (var i = 0; i < arr.length; i++) {
        total = total + arr[i];
    }
    return total;
}

console.log("1. "+sum(1, 10)); // 55
console.log("2. "+sum(5, 50, 2)) // 621
console.log("3. "+sum(15,10)) // 75
console.log("4. "+sum(20, 10, 2)) // 90
console.log("5. "+sum(1)) // 1
console.log("5. "+sum()) // 0 

console.log('\n===============================');
console.log('Soal No. 4 (Array Multidimensi)');
console.log('===============================\n');

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
            ] 

function datahandling(){
    
    for(var i = 0; i < input.length; i++) {
        var nomer_id    = input[i][0];
        var nama        = input[i][1];
        var ttl         = input[i][2]+' '+input[i][3];
        var hobi        = input[i][4];
        console.log("Nomor ID : "+nomer_id)
        console.log("Nama Lengkap : "+nama)
        console.log("TTL : "+ttl)
        console.log("Hobi : "+hobi,'\n')
    }
        
}

datahandling();

console.log('\n===============================');
console.log('Soal No. 5 (Balik Kata)');
console.log('===============================\n');

function balikKata(str) {
    var kata = "";
    for (var i = str.length - 1; i >= 0; i--) {
        kata += str[i];
    }
    return kata;
}

console.log("1. "+balikKata("Kasur Rusak")) // kasuR rusaK
console.log("2. "+balikKata("SanberCode")) // edoCrebnaS
console.log("3. "+balikKata("Haji Ijah")) // hajI ijaH
console.log("4. "+balikKata("racecar")) // racecar
console.log("5. "+balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\n===============================');
console.log('Soal No. 6 (Metode Array)');
console.log('===============================\n');

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(params){
    
    input[1] = 'Roman Alamsyah Elsharawy';
    
    input.splice(4, 1, "Pria", "SMA Internasional Metro") 
    console.log("1. ")
    console.log(input)    
    
    var tgllahir1 = input[3] 
    var tgllahir = tgllahir1.split("/")
    var tanggal_lahir = parseInt(tgllahir[0])
    var bulan_lahir = parseInt(tgllahir[1])
    var tahun_lahir = parseInt(tgllahir[2])
    switch(bulan_lahir) {
      case 1:   { var x = 'Januari' ; break; }
      case 2:   { var x = 'Februari' ; break; }
      case 3:   { var x = 'Maret' ; break; }
      case 4:   { var x = 'April' ; break; }
      case 5:   { var x = 'Mei' ; break; }
      case 6:   { var x = 'Juni' ; break; }
      case 7:   { var x = 'Juli' ; break; }
      case 8:   { var x = 'Agustus' ; break; }
      case 9:   { var x = 'September' ; break; }
      case 10:   { var x = 'Oktober' ; break; }
      case 11:   { var x = 'November' ; break; }
      case 12:   { var x = 'Desember' ; break; }
      default:  { var x = 'Bulan Tidak Valid'; }
    }
    console.log("2. " +x);
    
    console.log("3. ")
    tgllahir.sort(function (value1, value2) { return value2 - value1 } ) 
    console.log(tgllahir) 
    
    console.log("4. ")
    console.log(tanggal_lahir+" - "+bulan_lahir+' - '+tahun_lahir) 
    
    var nama = input[1]
    var name = nama.split(" ")
      
    console.log("5. ")
    console.log(name[0]+ " " +name[1])
}

dataHandling2(input);
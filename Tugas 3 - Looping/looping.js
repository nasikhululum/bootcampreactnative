
console.log('\n===================');
console.log('No. 1 Looping While');
console.log('===================\n');

console.log('LOOPING PERTAMA')

var nilai = 1;
var akhir = 20;
while( nilai <= akhir ){
	if(nilai%2==0){
		console.log(nilai+" - I Love Coding" );
   	} 
	nilai++;
}

console.log('\nLOOPING KEDUA')

var nilai_pertama = 20;
var nilai_akhir = 1;
while( nilai_pertama >= nilai_akhir ){
	if(nilai_pertama%2==0){
		console.log(nilai_pertama+" - I will become a mobile developer" );
   	} 
	nilai_pertama--;
}


console.log('\n=============================');
console.log('No. 2 Looping Menggunakan For');
console.log('=============================\n');

for(var angka = 1; angka <= 20; angka++){
  if((angka%3)===0 && (angka%2)==1){
	console.log(angka+ ' - I Love Coding' ); //kelipatan 3 DAN angka ganjil 
  }else if((angka%2)==1){
    console.log(angka+ ' - Santai' ); //Ganjil
  }else if ((angka%2)===0){
    console.log(angka+ ' - Berkualitas' ); //Genap
  }
}

console.log('\n================================');
console.log('No. 3 Membuat Persegi Panjang #');
console.log('================================\n');

var lebar = 8
var panjang = 4
var result = ''

for(i=1;i<=panjang;i++) {
	
    for(j=1;j<=lebar;j++) {
	  result = result + ' # ';	
    }
	
	console.log(result)
	var result='';
}

console.log('\n================================');
console.log('No. 4 Membuat Tangga');
console.log('================================\n');


var tangga = 7
var result = ''

for(i=1;i<=tangga;i++) {
	
    for(j=1;j<=i;j++) {
	  result = result + ' # ';	
    }
	
	console.log(result)
	var result='';
}

console.log('\n================================');
console.log('No. 5 Membuat Papan Catur');
console.log('================================\n');

var baris = 8
var kolom = 8
var awal = 0;
var result = ''

for (i = 0; i < baris; i++) {

    var hitam = awal;

    for (j = 0; j < kolom; j++) {

        if (hitam == 0) {
            var hitam = '#';
        } else {
            var hitam = ' ';
        }
        result = result + hitam;
    }

    if (awal == 0) {
        var awal = 1;
    } else {
        var awal = 0;
    }

    console.log(result)
    var result = '';

}
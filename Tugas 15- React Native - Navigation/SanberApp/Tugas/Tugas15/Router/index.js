import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../Pages/Home'
import AboutScreen from '../Pages/AboutScreen'
import AddScreen from '../Pages/AddScreen'
import LoginScreen from '../Pages/Login'
import ProjectScreen from '../Pages/ProjectScreen'
import SettingScreen from '../Pages/Setting'
import SkillScreen from '../Pages/SkillProject'

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login Screen" component={LoginScreen} />
                <Stack.Screen name="Home Screen" component={HomeScreen} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
        <Tab.Navigator>
            <Tab.Screen name="About" component={AboutScreen} />
            <Tab.Screen name="Add" component={AddScreen} />
            <Tab.Screen name="Skill" component={SkillScreen} />
        </Tab.Navigator>
)

const MyDrawwer = () => (
        <Drawwer.Navigator>
            <Drawwer.Screen name="App" component={MainApp} />
            <Drawwer.Screen name="Project" component={ProjectScreen} />
        </Drawwer.Navigator>
)    
    

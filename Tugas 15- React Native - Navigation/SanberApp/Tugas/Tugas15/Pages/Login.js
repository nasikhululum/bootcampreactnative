import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default function Login({navigation}) {
    return (
        <View style={styles.container}>
            <Text> Halaman Login </Text>
            <Button onPress={()=>navigation.navigate("MyDrawwer",{
                screen: 'App', params:{
                    screen:'About'
                }
            })} 
            
            title="Menuju Halaman Home "/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center"
    }
})

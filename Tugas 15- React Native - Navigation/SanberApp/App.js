
import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Tugas15 from './Tugas/Tugas15/Index';

export default function App() {
  return (
      <Tugas15 />
      
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

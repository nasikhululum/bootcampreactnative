
console.log('========\n');
console.log('TUGAS A');
console.log('\n========\n');

// 1. Soal No. 1 (Membuat kalimat).
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log('1. \n'+word.concat(' '+second+' '+third+' '+fourth+' '+fifth+' '+sixth+' '+seventh) ,'\n' );


// 2. Soal No.2 Mengurai kalimat (Akses karakter dalam string)

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // going 
var fourthWord = sentence[11] + sentence[12]; // to 
var fifthWord = sentence[14] + sentence[15]; // be 
var sixthWord = sentence[16] + sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] ; // react 
var seventhWord = sentence[22] + sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; // native 
var eighthWord = sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]; // Developer

console.log('2. \n'+'First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord ,'\n')

// 3. Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14) ; // JavaScript 
var thirdWord2 = sentence2.substring(15, 18) ; // is 
var fourthWord2 = sentence2.substring(18, 21) ; // so
var fifthWord2 = sentence2.substring(21, 26); // cool! 

console.log('3. \n'+'First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2, '\n');


// 4. Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14) ; // JavaScript 
var thirdWord3 = sentence3.substring(15, 17) ; // is 
var fourthWord3 = sentence3.substring(18, 20) ; // so
var fifthWord3 = sentence3.substring(21, 26); // cool! 

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('4. \n'+'First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength, '\n'); 

// B. Tugas Conditional
// 1. If-else
	
console.log('========\n');
console.log('TUGAS B');
console.log('\n========\n');

var nama = ""
var peran = ""

// 1.  
if(nama==''){
	console.log('1. \n\Nama = ' +nama)
	console.log('Peran = ' +peran)
	console.log('Nama harus diisi!','\n')
}

// 2.
var nama = "Jhon"
var peran = ""
if(nama!='' && peran==''){
	console.log('2. \n\Nama = ' +nama)
	console.log('Peran = ' +peran)
	console.log('Halo '+ nama +', Pilih peranmu untuk memulai game','\n')
}

// 3.

var nama = "Jane"
var peran = "penyihir"
if(nama!='' && peran=='penyihir'){
	console.log('3. \n\Nama = ' +nama)
	console.log('Peran = ' +peran)
	console.log("Selamat datang di Dunia Werewolf, "+nama)
	console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!",'\n')
}

// 4.

var nama = "Jenita"
var peran = "guard"
if(nama!='' && peran=='guard'){
	console.log('4. \n\Nama = ' +nama)
	console.log('Peran = ' +peran)
	console.log("Selamat datang di Dunia Werewolf, "+nama)
	console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf!",'\n')
}

// 5.

var nama = "Junaedi"
var peran = "werewolf"
if(nama!='' && peran=='werewolf'){
	console.log('5. \n\Nama = ' +nama)
	console.log('Peran = ' +peran)
	console.log("Selamat datang di Dunia Werewolf, "+nama)
	console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!",'\n')
}

// SWITCH CASE

console.log('6.');

// INPUT DISINI
var hari = parseInt('21');
var bulan = parseInt('01');
var tahun = parseInt('1945');

switch(bulan) {
  case 1:   { var x = 'Januari' ; break; }
  case 2:   { var x = 'Februari' ; break; }
  case 3:   { var x = 'Maret' ; break; }
  case 4:   { var x = 'April' ; break; }
  case 5:   { var x = 'Mei' ; break; }
  case 6:   { var x = 'Juni' ; break; }
  case 7:   { var x = 'Juli' ; break; }
  case 8:   { var x = 'Agustus' ; break; }
  case 9:   { var x = 'September' ; break; }
  case 10:   { var x = 'Oktober' ; break; }
  case 11:   { var x = 'November' ; break; }
  case 12:   { var x = 'Desember' ; break; }
  default:  { var x = 'Bulan Tidak Valid'; }
}
console.log("Inputan = " +hari+"-"+bulan+"-"+tahun);

if(hari>31 || hari<1){
	console.log("Tanggal yg dimasukkan Tidak Valid!")
	return false
}

if(bulan>12 || bulan<1){
	console.log("Bulan yg dimasukkan Tidak Valid!")
	return false
}

if(tahun>2200 || tahun<1900){
	console.log("Tahun yg dimasukkan Tidak Valid!")
	return false
}


console.log(hari+" "+x+" "+tahun);





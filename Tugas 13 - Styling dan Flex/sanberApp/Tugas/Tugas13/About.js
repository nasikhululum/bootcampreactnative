import React from 'react'
import { View, Text, Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput, Button } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Zocial } from '@expo/vector-icons'
import { FontAwesome } from '@expo/vector-icons'

export default function Login() {

    const nama = 'Nasikhul Ulum'
    const jobdesc = 'React Native developer'
    const facebook = (
                        <Icon.Button name="facebook" solid>
                            Facebook
                        </Icon.Button>
                    );
    const wa = (
                        <Icon.Button name="whatsapp" solid backgroundColor='green'>
                            08123456
                        </Icon.Button>
                    );        
    const email = (
                        <Zocial name="gmail" size={30} color="red" />
                    );      
                    
    const github = (  
                    <FontAwesome name="github" size={30} color="black" > Github </FontAwesome>
                    )

    const gitlab = (  
                        <FontAwesome name="gitlab" size={30} color="black" > Gitlab </FontAwesome>
                        )                

    return (
        <View style={styles.container}>
            
            <Text style={styles.headerTitle}>Tentang Saya</Text>

            <View style={styles.logoHeader}>
                <Image 
                    source={require('./asset/user.png')}
                />
            </View>

            <Text style={styles.nameTitle}>{nama}</Text>
            <Text style={styles.jobTitle}>{jobdesc}</Text>

            <View style={{paddingTop: 20}}>
                <View>
                    <Text style={styles.textaboveline}>Portofolio</Text>
                    <View style={styles.hairline} />
                </View>

                <View style={styles.detailPortofolio}>
                    <Text>{github}</Text>    
                    <Text>{gitlab}</Text>   
                </View>
            </View>
            
            <View style={{paddingTop: 20}}>
                <View>
                    <Text style={styles.textaboveline}>Contact</Text>
                    <View style={styles.hairline} />
                </View>

                <View style={styles.detailPortofolio}>
                    <Text>{facebook}</Text>    
                    <Text>{wa}</Text> 
                    <Text>{email}</Text>    
                </View>
            </View>
            
            
            

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    headerTitle:{
        paddingTop: 50,
        marginVertical: 30,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        fontSize: 18,
    },
    logoHeader: {
        height: 50,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 25
    },
    nameTitle: {
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        fontSize: 16,
    },
    jobTitle: {
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        fontSize: 15,
        paddingBottom: 15
    },

    detailPortofolio:{
        backgroundColor: '#EFEFEF',
        flexDirection: 'row',
        justifyContent: "space-evenly",
        paddingHorizontal: 10,
        marginHorizontal: 10,
        paddingVertical: 10,
        borderRadius:4
    },
    
    textaboveline: {
        fontSize: 14,
        paddingHorizontal: 10,
        color: 'black'
    },
    hairline: {
        left: 10,
        backgroundColor: '#A2A2A2',
        height: 2,
        width: 340
    },
      

  });
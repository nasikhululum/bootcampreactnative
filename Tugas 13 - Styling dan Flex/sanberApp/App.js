
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Login from './Tugas/Tugas13/Login';
import About from './Tugas/Tugas13/About';

export default function App() {
  return (
      <About />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

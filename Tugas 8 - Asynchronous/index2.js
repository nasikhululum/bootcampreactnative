var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timespent: 3000}, 
    {name: 'Fidas', timespent: 2000}, 
    {name: 'Kalkulus', timespent: 4000}
]

console.log('\n=========================================');
console.log('Soal No. 2 (Promise Baca Buku)');
console.log('=========================================\n');

const cekwaktubaca = (time, param)=>{ 
    
	param.forEach(function (b) {
		readBooksPromise(time, b)
		.then(function (fulfilled) {
            console.log(fulfilled);
        })
		.catch(function (error) {
            console.log(error.message);
        });
	
	});	
}

cekwaktubaca(10000, books)
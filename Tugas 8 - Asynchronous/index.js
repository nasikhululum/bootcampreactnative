var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

console.log('\n=========================================');
console.log('Soal No. 1 (Callback Baca Buku)');
console.log('=========================================\n');

const cekwaktubaca = (time, param)=>{ 
    
    param.forEach(b => readBooks(time, b, (cb) => {
      console.log(cb)
    }))
    
}

cekwaktubaca(10000, books)